# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d7ae45e19108e890d6501374320fc40f"

SRC_URI = "git://github.com/RobertBerger/ascii-invaders;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"
#SRCREV = "15da673ce250aba02bbcf54d16aff7479d6b89e5"


S = "${WORKDIR}/git/src"

DEPENDS = "ncurses"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

