/*
 *   Copyright (C) 2019 Robert Berger - Reliable Embedded Systems e.U.
 *   SPDX-License-Identifier: MIT
 */

#include <stdio.h>

int main(void) {
        /*~~~~~~~~~~~~~~~~~~~~~~~ */
        char buffer[16];
        volatile int i = 3456;
        /*~~~~~~~~~~~~~~~~~~~~~~~ */

        printf("the value of i= %d\n", i);
        buffer[16] = 3;      /* cppcheck */
        printf("the value if buffer[16] = %d\n", buffer[16]); /* cppcheck */

        for (i=0; i <= 16; i++) {
           buffer[i] = i;
           printf("buffer[%d]= %d\n", i, buffer[i]);
        }

        return (0);
}

/* Note:
    *) what happens if we do i = buffer[16] = 3;?
    *) how can we detect i = buffer[16] = 3;?
    *) what if we increment the index to buffer in a loop to 16?
*/
