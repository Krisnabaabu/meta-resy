# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "App container with oci tools"

require app-container-image.bb

# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
	oci-image-tools \
"
# skopeo \
# this gives:
# root@c44132cc31c9:/# skopeo --debug inspect docker://docker.io/reliableembeddedsystems/yocto
# DEBU[0000] reference rewritten from 'docker.io/reliableembeddedsystems/yocto:latest' to 'docker.io/reliableembeddedsystems/yocto:latest'
# DEBU[0000] Trying to pull "docker.io/reliableembeddedsystems/yocto:latest"
# DEBU[0000] Credentials not found
# DEBU[0000] Using registries.d directory /etc/containers/registries.d for sigstore configuration
# DEBU[0000]  No signature storage configuration found for docker.io/reliableembeddedsystems/yocto:latest
# DEBU[0000] Looking for TLS certificates and private keys in /etc/docker/certs.d/docker.io
# DEBU[0000] GET https://registry-1.docker.io/v2/
# DEBU[0000] Ping https://registry-1.docker.io/v2/ err Get https://registry-1.docker.io/v2/: x509: certificate signed by unknown authority (&url.Error{Op:"Get", URL:"https://registry-1.docker.io/v2/", Err:x509.UnknownAuthorityError{Cert:(*x509.Certificate)(0xc0001ad080), hintErr:error(nil), hintCert:(*x509.Certificate)(nil)}})
# DEBU[0000] GET https://registry-1.docker.io/v1/_ping
# DEBU[0000] Ping https://registry-1.docker.io/v1/_ping err Get https://registry-1.docker.io/v1/_ping: x509: certificate signed by unknown authority (&url.Error{Op:"Get", URL:"https://registry-1.docker.io/v1/_ping", Err:x509.UnknownAuthorityError{Cert:(*x509.Certificate)(0xc0001ae680), hintErr:error(nil), hintCert:(*x509.Certificate)(nil)}})
# FATA[0000] Error parsing image name "docker://docker.io/reliableembeddedsystems/yocto": error pinging docker registry registry-1.docker.io: Get https://registry-1.docker.io/v2/: x509: certificate signed by unknown authority
# root@c44132cc31c9:/#
#
