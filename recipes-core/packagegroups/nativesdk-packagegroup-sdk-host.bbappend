# those should end up in the nativesdk


RDEPENDS_${PN} += " \
    nativesdk-dtc \
    nativesdk-e2fsprogs \
    nativesdk-elfutils \
    nativesdk-lzop \
    nativesdk-make \
    nativesdk-mtd-utils \
    nativesdk-squashfs-tools \
    nativesdk-u-boot-tools \
    nativesdk-wget \
    nativesdk-xz \
    nativesdk-go-dep \
    nativesdk-go \
"
# we try to get nativesdk-agent-proxy via dynamic layers

#
#RDEPENDS_${PN} += " \
#    nativesdk-elftosb \
#    nativesdk-mxsldr \
#    nativesdk-u-boot-mkimage \
#    nativesdk-imx-usb-loader \
#    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'nativesdk-wayland', '', d)} \
#"


# nativesdk-git
# git clone https://github.com/influxdata/influxdb.git
#Cloning into 'influxdb'...
#fatal: unable to access 'https://github.com/influxdata/influxdb.git/': error setting certificate verify locations:
#  CAfile: /opt/resy/3.1/sysroots/x86_64-resysdk-linux/etc/ssl/certs/ca-certificates.crt
#  CApath: none

