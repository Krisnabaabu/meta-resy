DESCRIPTION = "Tools iot Package Group"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    ${PN}"

RDEPENDS_${PN} = "\
    mraa \
    upm \
    mosquitto \
    mosquitto-clients \
    softether \
    haveged \
    ntp \
    resolvconf \
    "

# resolvconf needed for ntp hack
# removed from above:
# rabbitmq-c
# python3-pip
# mysql-python
# python3-pyzmq


