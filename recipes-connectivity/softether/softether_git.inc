#
# Copyright (C) 2019 Robert Berger - Reliable Embedded Systems e.U.
# Copyright (C) 2019 SHIMADA Hirofumi - dejiko
# SPDX-License-Identifier: MIT
#

require softether.inc

# sca checks which apply to this recipe
inherit sca-c-checks

# only inherit sca if set in DISTRO_FEATURES
# inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

# in case we have forcecompile defined in DISTRO_FEATURES include a file, which enforces compile -> sca
require ${@ bb.utils.contains("DISTRO_FEATURES", "forcecompile", "conf/distro/include/force-compile.inc" , "", d)}

# this should put the softether-hamcorebuilder-native into our native-sysroot
DEPENDS += " zlib ncurses softether-hamcorebuilder-native virtual/libiconv readline openssl "

#RDEPENDS_${PN} += " virtual/libiconv readline openssl "
RDEPENDS_${PN} += " readline openssl "

# !!!! Make sure that in your image recipe at least you add !!!!
# glibc-gconv-utf-16
# otherwise you get funny/misleading errors like these:
# -- Alert: SoftEther VPN Kernel --
# String Library Init Failed.
# Please check your locale settings and iconv() libraries.

# get rid of "useless-rpaths" sanity errors:
EXTRA_OECMAKE += "\
  -DCMAKE_SKIP_RPATH=ON \
"
# I also tried to get rid of rpaths with something like this:
# find ${S}/build/vpnclient -type f -print0 | xargs -0 chrpath --delete
# but cmake magic requires to have rpaths and is unhappy without them
# it does:
#   file(RPATH_CHECK
#         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/libexec/softether/vpnclient/vpnclient"
#         RPATH "/usr/lib")

PACKAGES += " \
   ${PN}-vpnbridge \
   ${PN}-vpnclient \
   ${PN}-vpncmd \
   ${PN}-vpnserver \
   ${PN}-libs \
   ${PN}-systemd \
"
# ${PN}-dbg 
# ${PN}-list-cpu-features
# ${PN}-list-cpu-features-dev
# ${PN}-list-cpu-features-staticdev
# ${PN}-libs 

# we need to remopve the .so files from -dev
# so we can put them into -libs
FILES_SOLIBSDEV = ""

FILES_${PN}-libs = "\
  ${libdir}/libmayaqua.so \
  ${libdir}/libcedar.so \
"

# this RDEPENDS does not quite seem to work for some reason
# I added glibc-gconv-utf-16 to my image recipe, which seems to work
# otherwise you get funny/misleading errors like these:
# -- Alert: SoftEther VPN Kernel --
# String Library Init Failed.
# Please check your locale settings and iconv() libraries.
RDEPENDS_pn_${PN}-libs += "glibc-locale glibc-gconv-utf-16 glibc-gconv-euc-jp"

#INSANE_SKIP_${PN}-libs = "useless-rpaths"

FILES_${PN}-vpnbridge = "\
  ${bindir}/vpnbridge \
  ${libexecdir}/softether/vpnbridge/vpnbridge \
  ${libexecdir}/softether/vpnbridge/hamcore.se2 \
"

RDEPENDS_pn_${PN}-vpnbridge = "FILES_${PN}-libs"

#INSANE_SKIP_${PN}-vpnbridge = "useless-rpaths"

FILES_${PN}-vpnclient = "\
  ${bindir}/vpnclient \
  ${libexecdir}/softether/vpnclient/vpnclient \
  ${libexecdir}/softether/vpnclient/hamcore.se2 \
"

RDEPENDS_pn_${PN}-vpnclient = "FILES_${PN}-libs"

#INSANE_SKIP_${PN}-vpnclient = "useless-rpaths"

FILES_${PN}-vpncmd = "\
  ${bindir}/vpncmd \
  ${libexecdir}/softether/vpncmd/vpncmd \
  ${libexecdir}/softether/vpncmd/hamcore.se2 \
"

RDEPENDS_pn_${PN}-vpncmd = "FILES_${PN}-libs"

#INSANE_SKIP_${PN}-vpncmd = "useless-rpaths"

FILES_${PN}-vpnserver = "\
  ${bindir}/vpnserver \
  ${libexecdir}/softether/vpnserver/vpnserver \
  ${libexecdir}/softether/vpnserver/hamcore.se2 \
"
RDEPENDS_pn_${PN}-vpnserver = "FILES_${PN}-libs"

#INSANE_SKIP_${PN}-vpnserver = "useless-rpaths"

#FILES_${PN}-list-cpu-features = "\
#  ${bindir}/list_cpu_features- \
#"

#INSANE_SKIP_${PN}-list-cpu-features = "useless-rpaths"

#FILES_${PN}-hamcore = "\
#  ${libdir}/softether/hamcore.se2 \
#  ${libexecdir}/softether/vpnbridge/hamcore.se2 \
#  ${libexecdir}/softether/vpnclient/hamcore.se2 \
#  ${libexecdir}/softether/vpncmd/hamcore.se2 \
#  ${libexecdir}/softether/vpnserver/hamcore.se2 \
#"

#FILES_${PN}-list-cpu-features-dev = "\
#  ${includedir}/cpu_features/* \
#  ${libdir}/cmake/* \
#"

#FILES_${PN}-list-cpu-features-staticdev = "\
#  ${libdir}/libcpu_features.a \
#"

#!!!!
FILES_${PN}-dbg += "\
  ${exec_prefix}/lib/debug/* \
"

FILES_${PN}-src += "\
  ${exec_prefix}/src/debug/* \
"

FILES_${PN}-systemd = "\
  ${base_libdir}/systemd/* \
"

#FILES_${PN}-dev_remnove = "/usr/lib/libmayaqua.so"
#FILES_${PN}-dev_remnove = "/usr/lib/libcedar.so"
