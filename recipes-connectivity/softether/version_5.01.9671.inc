#
# Copyright (C) 2019 Robert Berger - Reliable Embedded Systems e.U.
# SPDX-License-Identifier: MIT
#

SRCREV = "24f426162cbbb55ee0fab40a8e3288ea443a3365"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a50d0469eb02a4651c9625d696b22a69"
