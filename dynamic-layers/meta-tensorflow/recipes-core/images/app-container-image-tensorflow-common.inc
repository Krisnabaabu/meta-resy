# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
	tensorflow \
"
