# search also in here
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://my-ntpd \
            file://my-ntp.conf \ 
"
do_install_append() {
    install -m 755 ${WORKDIR}/my-ntpd ${D}${sysconfdir}/init.d/ntpd
    install -m 644 ${WORKDIR}/my-ntp.conf ${D}${sysconfdir}/ntp.conf
}
