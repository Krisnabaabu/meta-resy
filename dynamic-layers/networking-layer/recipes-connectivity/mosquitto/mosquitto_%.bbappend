do_install_append() {
# user which runs mosquitto should be root instead of mosquitto for the right permission for log file
	sed -i 's/#user mosquitto/user root/' ${D}${sysconfdir}/mosquitto/mosquitto.conf
# define where we should log to
        sed -i 's/#log_dest stderr/log_dest file \/var\/log\/mosquitto.log/' ${D}${sysconfdir}/mosquitto/mosquitto.conf
# define what we should log
        sed -i 's/#log_type information/log_type all/' ${D}${sysconfdir}/mosquitto/mosquitto.conf
}
