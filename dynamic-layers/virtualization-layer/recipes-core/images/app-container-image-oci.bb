# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/common-img.inc

SUMMARY = "A minimal container image"

IMAGE_FSTYPES = "container oci"

inherit image
inherit image-oci

IMAGE_FEATURES = ""
IMAGE_LINGUAS = ""
NO_RECOMMENDATIONS = "1"

# Allow build with or without a specific kernel
# see poky/meta/classes/image-container.bbclass
IMAGE_CONTAINER_NO_DUMMY = "0"
# 0 -> no kernel, PREFERRED_PROVIDER_virtual/kernel =  linux-dummy
# 1 -> kernel,    PREFERRED_PROVIDER_virtual/kernel != linux-dummy

IMAGE_INSTALL = "\
	base-files \
	base-passwd \
	netbase \
"

# Workaround /var/volatile for now
ROOTFS_POSTPROCESS_COMMAND += "rootfs_fixup_var_volatile ; "

rootfs_fixup_var_volatile () {
	install -m 1777 -d ${IMAGE_ROOTFS}/${localstatedir}/volatile/tmp
	install -m 755  -d ${IMAGE_ROOTFS}/${localstatedir}/volatile/log
}
