# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A lighttpd container image"

require dynamic-layers/virtualization-layer/recipes-core/images/app-container-image-oci.bb
require dynamic-layers/meta-java/recipes-core/images/app-container-image-java-common.inc 
